package de.binaris.HaraldBaseball;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import lejos.remote.ev3.RemoteRequestPilot;


@ExtendWith(MockitoExtension.class)
public class PilotTest {

	@Mock
	private RemoteRequestPilot pilot;
	
	@Test
	public void test() {
		assertThat(pilot).isNotNull();
	}
}
